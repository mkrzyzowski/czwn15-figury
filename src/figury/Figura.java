package figury;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.util.Random;

/**
 * @author tb
 *
 */
public abstract class Figura implements Runnable, ActionListener {
	Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	// wspolny bufor
	protected Graphics2D buffer;
	protected Area area;
	// do wykreslania
	protected Shape shape;
	// przeksztalcenie obiektu
	protected AffineTransform aft;

	// przesuniecie
	private int dx, dy;
	// rozciaganie
	private double sf;
	// kat obrotu
	private double an;
	private int delay;
	private double width;
	private double height;
	private Color clr;
	private boolean z;
	private int tempdx=dx;
	private int tempdy=dy;

	protected static final Random rand = new Random();

	public Figura(Graphics2D buf, int del, double w, double h,boolean z_) {
		delay = del;
		buffer = buf;
		width = w;
		height = h;
		z=z_;

		dx = 1 + rand.nextInt(4);
		dy = 1 + rand.nextInt(4);
		tempdx=dx;
		tempdy=dy;
		sf = 1 + 0.1 * rand.nextDouble();
		an = 0.1 * rand.nextDouble();
		clr = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
		// reszta musi by� zawarta w realizacji klasy Figure
		// (tworzenie figury i przygotowanie transformacji)

	}

	@Override
	public void run() {
		// miejsce rozpoczecia rysowania figur
		aft.translate(30, 30);
		area.transform(aft);
		shape = area;
		while (true) {
			// przygotowanie nastepnego kadru
			shape = nextFrame();
			try {
				synchronized(Thread.currentThread())
				{
				if(z)
					{
						dx=0;
						dy=0;
					}
				else
				{
					Thread.sleep(delay);
					dx=tempdx;
					dy=tempdy;
				}
				}
			} catch (InterruptedException e) {
			}
		}
	}
	void stopthreads(boolean y)
	{
		z=y;
	}
	protected Shape nextFrame() {
		// zapamietanie na zmiennej tymczasowej
		// aby nie przeszkadzalo w wykreslaniu
		area = new Area(shape);
		aft = new AffineTransform();
		Rectangle2D bounds = area.getBounds2D();
		double cx = bounds.getCenterX();
		double cy = bounds.getCenterY();
		// odbicie
		if (bounds.getX() <= 10)
		{
			dx = tempdx= -dx;
			aft.translate(30, 0);
		}
		if(bounds.getMaxX() > screen.width-bounds.getWidth())
		{
			dx = tempdx= -dx;
			aft.translate(-30, 0);
		}
		if (bounds.getY() <= 10) 
		{
			dy = tempdy = -dy;
			aft.translate(0, 30);
		}
		if(bounds.getMaxY() > screen.height-bounds.getHeight()-100)
		{
			dy=tempdy=-dy;
			aft.translate(0, -30);
		}
		// zwiekszenie lub zmniejszenie
		// konstrukcja przeksztalcenia
		aft.translate(cx, cy);
		if((0<cy-bounds.getHeight()-25 && cy+bounds.getHeight()<height-25)&&(0 < cx-bounds.getWidth()-25 && cx+bounds.getWidth()<width-25))
		{
			if (bounds.getHeight() > 50)
				{
					aft.scale(0.7, 0.7);
					sf=1/sf;
				}
			else if(bounds.getHeight()<5)
				{
					aft.scale(2.2, 2.2);
					sf=1/sf;
				}
			else
				aft.scale(sf, sf);
		}
	    aft.rotate(an);
		aft.translate(-cx, -cy);
		aft.translate(dx, dy);
		// przeksztalcenie obiektu
		area.transform(aft);
		return area;
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		// wypelnienie obiektu
		buffer.setColor(clr.brighter());
		buffer.fill(shape);
		// wykreslenie ramki
		buffer.setColor(clr.darker());
		buffer.draw(shape);
	}

}
