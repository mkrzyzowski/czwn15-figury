package figury;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;

public class Kwadrat extends Figura
{
	public Kwadrat(Graphics2D g,int x, double a_, double b_,boolean z_)
	{
		super(g,x,a_,b_, z_);
		shape=new Rectangle2D.Double(0,0,10,10);
		aft = new AffineTransform();
		area = new Area(shape);
	}
}