package figury;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.awt.event.ActionEvent;

public class AnimatorApp extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	protected List<Figura>Lista=new ArrayList<>();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					final AnimatorApp frame = new AnimatorApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	int number = 0;
	
	public AnimatorApp() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(0,0,screen.width, screen.height);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		AnimPanel kanwa = new AnimPanel();
		kanwa.setBounds(0,0,screen.width, screen.height-120);
		contentPane.add(kanwa);
		
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				kanwa.initialize();
			}
		});
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				Lista.add(kanwa.addFig());
			}
		});
		btnAdd.setBounds(screen.width/2-80, screen.height-90, 80, 23);
		contentPane.add(btnAdd);
		setBackground(Color.WHITE);
		
		JButton btnAnimate = new JButton("Animate");
		btnAnimate.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e)
			{
				kanwa.animate();
				if(number%2!=0)
				{
					for(int i=0;i<Lista.size();i++)
						Lista.get(i).stopthreads(true);
				}
				else
				{
					for(int i=0;i<Lista.size();i++)
						Lista.get(i).stopthreads(false);
				}
				number++;
			}		
		});
		btnAnimate.setBounds(screen.width/2+5, screen.height-90, 80, 23);
		contentPane.add(btnAnimate);
	}
}