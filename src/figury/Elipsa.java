package figury;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;

public class Elipsa extends Figura
{
	public Elipsa(Graphics2D buf, int del, double w, double h,boolean z_) 
	{
		super(buf, del, w, h,z_);
		shape=new Ellipse2D.Double(0,0,20,10);
		aft = new AffineTransform();
		area=new Area(shape);
	}

}